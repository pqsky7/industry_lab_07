package ictgradschool.industry.lab07.ex06;

import ictgradschool.Keyboard;

/**
 * TODO Write a program according to the Exercise Six guidelines in your lab handout.
 */
public class ExerciseSix {

    public void start() {
        // TODO Write the codes :)
        String input = getUserInput();
        try {
            checkLength(input);
            String results = getFirstChar(input);
            System.out.println("You entered:" + results);
        } catch (ExceedMaxStringLengthException e) {
            System.out.println("Error: invalid string length");
        } catch (InvalidWordException e) {
            System.out.println("Error: non-word detected");
        }
    }

    // TODO Write some methods to help you.
    public String getUserInput() {
        System.out.print("Enter a string of at most 100 characters: ");
        return Keyboard.readInput();
    }

    public String getFirstChar(String input) throws InvalidWordException {
        input = " " + input;
        String firstChar = "";
        int indexFrom = -1;
        while(true) {
            indexFrom = input.indexOf(' ', indexFrom + 1);
            if (indexFrom ==-1){
                break;
            }
            if (input.charAt(indexFrom + 1) != ' '){
                checkWorld(input.charAt(indexFrom + 1));
                firstChar += " " + input.charAt(indexFrom + 1);
            }
        }
        return firstChar;
    }

    public void checkLength(String input) throws ExceedMaxStringLengthException {
        if (input.length() > 100 || input.length() < 1) {
            throw new ExceedMaxStringLengthException();
        }
    }

    public void checkWorld(char input) throws InvalidWordException {
        if (!Character.isLetter(input)) {
            throw new InvalidWordException();
        }
    }


    public class ExceedMaxStringLengthException extends Exception {
        public ExceedMaxStringLengthException() {
        }
    }

    public class InvalidWordException extends Exception {
        public InvalidWordException() {
        }
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseSix().start();
    }
}
